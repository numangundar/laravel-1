@extends('layout/main')

@section('title', 'Daftar Student')




@section('container')

<div class="container">
  <div class="row">
    <div class="col-6">
    <h1 class="mt-3">Daftar Students</h1>

  <a href="/students/create" class="btn btn-primary my-3">Tambah Data Student</a>
   
    @if (session('status'))
    <div class="alert alert-succes">
    {{session('status') }}
    </div>
    @endif

    
<ul class="list-group">
   @foreach( $students as $student) 

  <li class="list-group-item d-flex justify-content-between align-items-center">
    
    {{$student-> nama}}
   <span class="badge bg-info"> <a href="/students/{{$student->id}}" class= "badge badge-primary stretched link"> detail </a> </span>
  </li>
  @endforeach
</ul>

    </div>
  </div> 
</div>

    <!-- Optional JavaScript; choose one of the two! -->

 @endsection
