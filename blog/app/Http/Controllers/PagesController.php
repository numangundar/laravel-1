<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PagesController extends Controller
{
    public function home()
    {
        return view('index');
    }

    public function about()
    {
        return view('about', ['nama' => 'About ini dari controller']);
    }

    public function sapa()
    {
        return "Hallo";
    }

    public function sapa_post(Request $request)
    {
        $nama = $request["nama1"];
        $lnama = $request["nama2"];
        return "<h1>Selamat datang di permulaan Sanbarcode $nama $lnama, Tetap semangat yaa!!!</h1>";
    }
}
